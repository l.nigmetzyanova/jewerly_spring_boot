package kpfu.itis.sem.repository;

import kpfu.itis.sem.entity.WishesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface WishRepository extends JpaRepository<WishesEntity, UUID> {
}
