package kpfu.itis.sem.repository;

import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, UUID> {

    @Query("SELECT p, p.isAvailable FROM ProductEntity p WHERE p.id in " +
            "(SELECT d.productId FROM DescriptionEntity d WHERE d.insertion =: insertion)")
    HashMap<ProductEntity, Boolean> getAvailableByInsertion(@Param("insertion") String insertion);

    @Query("SELECT p FROM ProductEntity p WHERE p.isAvailable = true  AND p.id in " +
            "(SELECT d.productId FROM DescriptionEntity d WHERE d.metal =: metal)")
    List<ProductEntity> getAvailableByMetal(@Param("metal") String metal);





}
