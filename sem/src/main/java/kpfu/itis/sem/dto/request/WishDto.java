package kpfu.itis.sem.dto.request;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class WishDto {

    private String wish;

    private UUID id;

}
