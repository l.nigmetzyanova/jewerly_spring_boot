package kpfu.itis.sem.dto.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Builder
public class OrderFormDto {

    private String customerName;

    private String phone;

    private String address;

    private String comment;


}
