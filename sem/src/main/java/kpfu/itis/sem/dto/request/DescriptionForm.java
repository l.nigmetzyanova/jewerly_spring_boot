package kpfu.itis.sem.dto.request;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DescriptionForm {

    private Integer minWeight;

    private String metal;

    private String insertion;

    private String mechanism;

    private String gender;

    private String productId;



}
