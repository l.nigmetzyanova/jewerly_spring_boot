package kpfu.itis.sem.dto.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CustomerFormDto {

    private String name;

    private String phoneNumber;
}
