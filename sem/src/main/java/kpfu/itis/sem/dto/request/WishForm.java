package kpfu.itis.sem.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WishForm {

    private String wish;

}
