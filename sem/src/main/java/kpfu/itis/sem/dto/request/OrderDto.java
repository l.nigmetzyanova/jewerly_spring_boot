package kpfu.itis.sem.dto.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class OrderDto {

    private UUID id;

    private String customerName;

    private Integer countProduct;

    private Integer cost;

    private String status;

    private String address;

    private String comment;
}
