package kpfu.itis.sem.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Setter
@SuperBuilder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WishesEntity extends AbstractEntity{

    @Column(nullable = false)
    private String wish;

}
