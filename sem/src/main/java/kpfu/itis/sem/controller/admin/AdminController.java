package kpfu.itis.sem.controller.admin;


import kpfu.itis.sem.dto.request.DescriptionDto;
import kpfu.itis.sem.dto.request.DescriptionForm;
import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.entity.ProductEntity;
import kpfu.itis.sem.service.impl.BaseDescriptionService;
import kpfu.itis.sem.service.impl.BaseOrderService;
import kpfu.itis.sem.service.impl.BaseProductService;
import kpfu.itis.sem.util.mapper.impl.BaseListDescriptionMapper;
import kpfu.itis.sem.util.mapper.impl.BaseListProductMapper;
import kpfu.itis.sem.util.mapper.DescriptionMapper;
import kpfu.itis.sem.util.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final BaseProductService productService;
    private final ProductMapper mapper;
    private final BaseListProductMapper listProductMapper;
    private final BaseDescriptionService descriptionService;
    private final DescriptionMapper descriptionMapper;
    private final ProductMapper productMapper;
    private final BaseListDescriptionMapper listDescriptionMapper;
    private final BaseOrderService orderService;


    @GetMapping("/showAdmin")
    public String getAdminPage(Model model) {
        List<ProductEntity> allProducts = productService.showAllProducts();
        List<ProductsDto> products = listProductMapper.mupStruct(allProducts);
        ProductsDto productForm = ProductsDto.builder().build();
        List<OrderDto> orders = orderService.listToDto(orderService.getAll());
        model.addAttribute("orders", orders);
        model.addAttribute("products", products);
        List<DescriptionDto> descriptionDtos = listDescriptionMapper.toDtoList(descriptionService.getDescriptions());
        model.addAttribute("descriptions", descriptionDtos);
        model.addAttribute("productForm", productForm);
        model.addAttribute("descriptionForm", DescriptionForm.builder().build());
        return "admin";
    }

    @PostMapping("/addProduct")
    public String addNewProduct(@ModelAttribute("productForm") ProductsDto productsDto) {
        productService.createProduct(mapper.toEntity(productsDto));
        return "redirect:/admin/showAdmin";
    }

    @PostMapping("/addDescription")
    public String addNewDescription(@ModelAttribute("descriptionForm") DescriptionForm descriptionForm) {
        UUID productId = UUID.fromString(descriptionForm.getProductId());
        ProductEntity product = productService.getProductById(productId);
        DescriptionEntity descriptionEntity = descriptionService.getDescriptionByProductId(product);
        if (descriptionEntity != null) {
            descriptionService.update(descriptionEntity.getId(), descriptionForm);
        } else {
            DescriptionEntity description = descriptionMapper.toEntityFromForm(descriptionForm);
            description.setProductId(product);
            descriptionService.save(description);
        }
        return "redirect:/admin/showAdmin";
    }

    @PostMapping("/deleteProduct")
    public String deleteProducts(@RequestParam("HighlightedProducts") List<String> ids) {
        if (ids != null) {
            for (String id : ids) {
                UUID productId = UUID.fromString(id);
                productService.deleteProduct(productId);
            }
        }
        return "redirect:/admin/showAdmin";
    }

    @PostMapping("/deleteDescription")
    public String deleteDescriptions(@RequestParam("HighlightedDescriptions") List<String> ids) {
        if (ids != null) {
            for (String id : ids) {
                UUID descriptionId = UUID.fromString(id);
                descriptionService.deleteDescriptionById(descriptionId);
            }
        }
        return "redirect:/admin/showAdmin";
    }

    @GetMapping("/updateProduct/{id}")
    public String showUpdatePage(Model model, @PathVariable String id) {
        model.addAttribute("productForUpdate", productService.getProductById(UUID.fromString(id)));
        model.addAttribute("productFormForUpdate", ProductsDto.builder().build());
        return "updateProduct";
    }

    @PostMapping("/updateProduct/{id}")
    public String updateProduct(Model model, @ModelAttribute("productFormForUpdate") ProductsDto productDto,
                                @PathVariable String id) {
        productService.updateProduct(UUID.fromString(id), productMapper.toEntity(productDto));
        return "redirect:/admin/showAdmin";
    }

    @PostMapping("/updateOrderStatus")
    public String updateOrderStatus(@RequestParam("HighlightedOrders") List<String> ids) {
        if (ids != null) {
            for (String id : ids) {
                UUID orderId = UUID.fromString(id);
                orderService.updateStatusById(orderId);
            }
        }
        return "redirect:/admin/showAdmin";
    }

}
