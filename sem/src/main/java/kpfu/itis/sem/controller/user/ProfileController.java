package kpfu.itis.sem.controller.user;


import kpfu.itis.sem.dto.request.*;
import kpfu.itis.sem.entity.CustomerEntity;
import kpfu.itis.sem.security.details.BaseUserDetails;
import kpfu.itis.sem.service.impl.BaseCustomerService;
import kpfu.itis.sem.service.impl.BaseOrderService;
import kpfu.itis.sem.service.impl.BaseUserService;
import kpfu.itis.sem.util.mapper.CustomerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/profile")
@Controller
@RequiredArgsConstructor
public class ProfileController {

    private final CustomerMapper customerMapper;

    private final BaseCustomerService customerService;

    private final BaseUserService userService;

    private final BaseOrderService orderService;

    @GetMapping
    public String getProfilePage(Model model, Authentication authentication) {
        BaseUserDetails user = (BaseUserDetails) authentication.getPrincipal();
        CustomerEntity customerEntity = customerService.getCustomerByUserId(userService
                .findUserByName(user.getUsername()).get().getId());
        CustomerDto customerDto = customerMapper.toDto(customerEntity);
        if (customerDto != null) {
            List<OrderDto> orders = orderService.listToDto(orderService.getOrdersByCustomerId(customerDto.getId()));
            model.addAttribute("orders", orders);
            model.addAttribute("customer", customerDto);
        } else {
            model.addAttribute("customer", CustomerDto.builder().build());
        }

        return "profile";
    }

    @PostMapping
    public String addInfoAboutCustomer(@RequestParam ("name") String name,
                                       @RequestParam ("phone") String phone,
                                       Authentication authentication) {
        CustomerEntity customer = customerService
                .createCustomerByNameAndPhone(name, phone);
        BaseUserDetails user = (BaseUserDetails) authentication.getPrincipal();
        customer.setUserId(userService.findUserByName(user.getUsername()).get());
        CustomerEntity customerEntity = customerService
                .getCustomerByUserId(userService.findUserByName(user.getUsername()).get().getId());
        if (customerEntity != null) {
            customerService.updateCustomer(customerEntity.getId(), name, phone);
        } else {
            customerService.createCustomer(customer);
        }

        return "redirect:/profile";
    }
}
