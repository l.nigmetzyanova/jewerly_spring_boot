package kpfu.itis.sem.controller.product;

import kpfu.itis.sem.dto.request.CustomerDto;
import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.dto.request.OrderFormDto;
import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.CustomerEntity;
import kpfu.itis.sem.entity.OrderEntity;
import kpfu.itis.sem.security.details.BaseUserDetails;
import kpfu.itis.sem.service.impl.BaseCustomerService;
import kpfu.itis.sem.service.impl.BaseOrderProductService;
import kpfu.itis.sem.service.impl.BaseOrderService;
import kpfu.itis.sem.service.impl.BaseUserService;
import kpfu.itis.sem.util.mapper.CustomerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final BaseOrderService orderService;

    private final BaseCustomerService customerService;

    private final BaseUserService userService;

    private final CustomerMapper customerMapper;

    private final BaseOrderProductService orderProductService;

    @GetMapping("/get")
    public String showOrder(Model model, HttpSession session, Authentication authentication) {
        if (authentication != null) {
            BaseUserDetails user = (BaseUserDetails) authentication.getPrincipal();

            CustomerEntity customerEntity = customerService.getCustomerByUserId(userService
                    .findUserByName(user.getUsername()).get().getId());
            CustomerDto customerDto = customerMapper.toDto(customerEntity);
            if (customerDto != null) {
                model.addAttribute("customer", customerDto);
            } else {
                model.addAttribute("customer", CustomerDto.builder().build());
            }
            HashMap<ProductsDto, Integer> cart = (HashMap<ProductsDto, Integer>) session.getAttribute("cart");
            if (cart != null) {
                model.addAttribute("cart", cart);
                OrderFormDto orderInfo = OrderFormDto.builder().build();
                model.addAttribute("orderInfo", orderInfo);
            }
            return "order";

        } else {
            return "redirect:/signIn";
        }
    }

    @PostMapping("/save")
    public String saveOrder(HttpSession session,
                            @ModelAttribute("orderInfo") OrderFormDto orderFormDto, Authentication authentication) {
        HashMap<ProductsDto, Integer> cart = (HashMap<ProductsDto, Integer>) session.getAttribute("cart");
        if (cart != null) {
            BaseUserDetails user = (BaseUserDetails) authentication.getPrincipal();
            OrderEntity order = orderService.createEntityByOrderInfo(orderFormDto, cart);
            CustomerEntity customerEntity = customerService.getCustomerByUserId(userService
                    .findUserByName(user.getUsername()).get().getId());
            order.setCustomerId(customerEntity);
            orderService.createOrder(order);
            orderProductService.saveOrderProduct(order, cart);
            session.removeAttribute("cart");
        }
        return "redirect:/profile";
    }
}
