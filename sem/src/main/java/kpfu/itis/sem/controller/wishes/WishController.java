package kpfu.itis.sem.controller.wishes;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.dto.request.WishForm;
import kpfu.itis.sem.service.impl.BaseWishService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/wish")
@Tags(value = {
        @Tag(name = "wishes")
})
public class WishController {

    private final BaseWishService wishService;

    @Operation(summary = "Получение пожеланий")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Продукты получены"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @GetMapping("/allWishes")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<WishDto>> getWishes() {
        List<WishDto> wishes = wishService.getWishes();

        return ResponseEntity.ok(wishes);
    }

    @Operation(summary = "Создание пожеланий")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пожелание создано"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<WishDto> addWish(@RequestBody WishForm wishForm) {
        WishDto wishDto = wishService.createWish(wishForm);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(wishDto);
    }

    @Operation(summary = "Получение пожелания по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пожелание получено"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/get/{id}")
    public ResponseEntity<WishDto> getWish(@PathVariable UUID id) {
        WishDto wishDto = wishService.getWishById(id);
        return ResponseEntity.ok(wishDto);
    }

    @Operation(summary = "Обновление пожеланий")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пожелание обновлено"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<WishDto> updateWish(@PathVariable UUID id, @RequestBody WishForm wishForm) {
        WishDto wishDto = wishService.updateWish(id, wishForm);
        return ResponseEntity.accepted().body(wishDto);
    }

    @Operation(summary = "Удаление пожеланий")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пожелание удалено"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteWish(@PathVariable UUID id) {
        wishService.deleteById(id);
        return ResponseEntity.accepted().build();
    }




}
