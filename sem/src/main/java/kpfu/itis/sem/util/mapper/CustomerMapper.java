package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.CustomerDto;
import kpfu.itis.sem.entity.CustomerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface CustomerMapper {

    CustomerEntity toEntity(CustomerDto customerDto);

    CustomerDto toDto(CustomerEntity entity);

}
