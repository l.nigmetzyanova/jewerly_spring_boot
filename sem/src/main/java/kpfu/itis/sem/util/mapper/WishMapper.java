package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.entity.WishesEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WishMapper {

    WishDto toDto(WishesEntity entity);

}
