package kpfu.itis.sem.util.mapper.impl;

import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.entity.OrderEntity;
import kpfu.itis.sem.util.mapper.ListOrderMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BaseListOrderMapper implements ListOrderMapper {

    @Override
    public List<OrderDto> mupStruct(List<OrderEntity> orders) {
        List<OrderDto> orderDtos = new ArrayList<>();
        for (OrderEntity order : orders) {
            OrderDto orderDto = OrderDto.builder()
                    .cost(order.getCost())
                    .address(order.getAddress())
                    .comment(order.getComment())
                    .countProduct(order.getCountProduct())
                    .id(order.getId())
                    .customerName(order.getCustomerName())
                    .status(order.getStatus())
                    .build();
            orderDtos.add(orderDto);
        }
        return orderDtos;    }
}
