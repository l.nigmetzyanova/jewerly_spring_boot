package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.entity.OrderEntity;

import java.util.List;

public interface ListOrderMapper {

    List<OrderDto> mupStruct(List<OrderEntity> orders);


}
