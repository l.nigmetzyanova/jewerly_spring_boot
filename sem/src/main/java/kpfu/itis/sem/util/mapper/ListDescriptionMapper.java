package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.DescriptionDto;
import kpfu.itis.sem.entity.DescriptionEntity;

import java.util.List;

public interface ListDescriptionMapper {

    List<DescriptionDto> toDtoList(List<DescriptionEntity> descriptionEntities);

}
