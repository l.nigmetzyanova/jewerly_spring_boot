package kpfu.itis.sem.util.mapper.impl;

import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.entity.WishesEntity;
import kpfu.itis.sem.util.mapper.ListWishMapper;
import kpfu.itis.sem.util.mapper.WishMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class BaseListWishMapper implements ListWishMapper {

    private final WishMapper wishMapper;

    @Override
    public List<WishDto> listToDto(List<WishesEntity> entities) {
        List<WishDto> wishDtos = new ArrayList<>();
        for (WishesEntity entity: entities) {
            wishDtos.add(wishMapper.toDto(entity));
        }
        return wishDtos;
    }
}
