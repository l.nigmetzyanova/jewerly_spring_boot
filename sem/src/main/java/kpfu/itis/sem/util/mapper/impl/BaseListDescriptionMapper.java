package kpfu.itis.sem.util.mapper.impl;

import kpfu.itis.sem.dto.request.DescriptionDto;
import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.util.mapper.DescriptionMapper;
import kpfu.itis.sem.util.mapper.ListDescriptionMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class BaseListDescriptionMapper implements ListDescriptionMapper {

    private final DescriptionMapper descriptionMapper;

    @Override
    public List<DescriptionDto> toDtoList(List<DescriptionEntity> descriptionEntities) {
        List<DescriptionDto> descriptionDtos = new ArrayList<>();
        for (DescriptionEntity descriptionEntity: descriptionEntities) {
            descriptionDtos.add(descriptionMapper.toDto(descriptionEntity));
        }
        return descriptionDtos;
    }
}
