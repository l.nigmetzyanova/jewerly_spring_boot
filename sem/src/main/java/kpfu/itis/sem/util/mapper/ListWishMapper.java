package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.entity.WishesEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ListWishMapper {
    List<WishDto> listToDto(List<WishesEntity> entities);
}
