package kpfu.itis.sem.util.mapper;

import kpfu.itis.sem.dto.request.DescriptionDto;
import kpfu.itis.sem.dto.request.DescriptionForm;
import kpfu.itis.sem.entity.DescriptionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DescriptionMapper {

    @Mapping(target = "createDate", ignore = true)
    DescriptionDto toDto(DescriptionEntity entity);

    @Mapping(target = "productId", ignore = true)
    DescriptionEntity toEntityFromForm(DescriptionForm form);

}
