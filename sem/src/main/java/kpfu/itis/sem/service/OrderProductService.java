package kpfu.itis.sem.service;

import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.OrderEntity;
import kpfu.itis.sem.entity.OrderProduct;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public interface OrderProductService {

    void createOrderProduct(OrderProduct orderProduct);

    void saveOrderProduct(OrderEntity order, HashMap<ProductsDto, Integer> cart);


}
