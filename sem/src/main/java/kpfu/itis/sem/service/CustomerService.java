package kpfu.itis.sem.service;

import kpfu.itis.sem.dto.request.OrderFormDto;
import kpfu.itis.sem.entity.CustomerEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public interface CustomerService {

    void createCustomer(CustomerEntity customer);

    void updateCustomer(UUID id, String name, String phone);

    CustomerEntity getCustomerByUserId(UUID id);

    CustomerEntity getCustomerById(UUID id);

    CustomerEntity createCustomerByNameAndPhone(String name, String phone);

}
