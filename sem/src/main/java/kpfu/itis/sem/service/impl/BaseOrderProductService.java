package kpfu.itis.sem.service.impl;


import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.OrderEntity;
import kpfu.itis.sem.entity.OrderProduct;
import kpfu.itis.sem.repository.OrderProductRepository;
import kpfu.itis.sem.service.OrderProductService;
import kpfu.itis.sem.util.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseOrderProductService implements OrderProductService {

    private final OrderProductRepository orderProductRepository;

    private final ProductMapper mapper;

    @Override
    public void createOrderProduct(OrderProduct orderProduct) {
        orderProductRepository.save(orderProduct);
    }

    @Override
    public void saveOrderProduct(OrderEntity order, HashMap<ProductsDto, Integer> cart) {
        for (Map.Entry<ProductsDto, Integer> entry : cart.entrySet()) {
            OrderProduct record = OrderProduct.builder()
                    .productId(mapper.toEntity(entry.getKey()))
                    .count(entry.getValue())
                    .orderId(order)
                    .build();
            createOrderProduct(record);
        }


    }
}
