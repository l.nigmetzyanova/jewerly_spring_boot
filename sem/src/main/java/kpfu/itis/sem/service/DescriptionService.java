package kpfu.itis.sem.service;

import kpfu.itis.sem.dto.request.DescriptionForm;
import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.entity.ProductEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface DescriptionService {

    DescriptionEntity getDescriptionByProductId(UUID id);

    void save(DescriptionEntity description);

    void update(UUID id, DescriptionForm descriptionForm);

    List<DescriptionEntity> getDescriptions();

    void deleteDescriptionById(UUID id);

    DescriptionEntity getDescriptionByProductId(ProductEntity entity);

}
