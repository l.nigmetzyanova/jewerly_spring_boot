package kpfu.itis.sem.service.impl;

import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.dto.request.WishForm;
import kpfu.itis.sem.entity.WishesEntity;
import kpfu.itis.sem.repository.WishRepository;
import kpfu.itis.sem.service.WishService;
import kpfu.itis.sem.util.mapper.WishMapper;
import kpfu.itis.sem.util.mapper.impl.BaseListWishMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseWishService implements WishService {

    private final WishRepository wishRepository;

    private final WishMapper wishMapper;

    private final BaseListWishMapper listWishMapper;

    @Override
    public List<WishDto> getWishes() {
        return listWishMapper.listToDto(wishRepository.findAll());
    }

    @Override
    public WishDto createWish(WishForm wishForm) {
        WishesEntity wishesEntity = WishesEntity.builder().wish(wishForm.getWish()).build();
        wishRepository.save(wishesEntity);
        return wishMapper.toDto(wishesEntity);
    }

    @Override
    public WishDto getWishById(UUID id) {
        return wishMapper.toDto(wishRepository.findById(id).
                orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Description not found")));
    }

    @Override
    public WishDto updateWish(UUID id, WishForm wishForm) {
        WishesEntity wish = wishRepository.findById(id).
                orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Description not found"));
        wish.setWish(wishForm.getWish());
        wishRepository.save(wish);
        return wishMapper.toDto(wish);
    }

    @Override
    public void deleteById(UUID id) {
        wishRepository.deleteById(id);
    }
}
