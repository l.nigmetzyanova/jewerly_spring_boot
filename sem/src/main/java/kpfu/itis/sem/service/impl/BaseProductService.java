package kpfu.itis.sem.service.impl;


import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.entity.ProductEntity;
import kpfu.itis.sem.repository.DescriptionRepository;
import kpfu.itis.sem.repository.ProductRepository;
import kpfu.itis.sem.service.ProductService;
import kpfu.itis.sem.util.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseProductService implements ProductService {


    private final ProductMapper productMapper;

    private final ProductRepository productRepository;

    private final DescriptionRepository descriptionRepository;

    @Override
    public List<ProductEntity> showAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void createProduct(ProductEntity productEntity) {
        productRepository.save(productEntity);
    }

    @Override
    public ProductEntity getProductById(UUID id) {
        return productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Description not found"));
    }

    @Override
    public ProductsDto getProductsDtoById(UUID id) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        if (productEntityOptional.isPresent()) {
            ProductEntity product = productEntityOptional.get();

            ProductsDto productsDto = productMapper.toDto(product);
            return productsDto;
        }
        return null;
    }

    @Override
    public void updateProduct(UUID id, ProductEntity productEntity) {
        ProductEntity product = productRepository.getReferenceById(id);
        product.setPictures(productEntity.getPictures());
        product.setCategory(productEntity.getCategory());
        product.setName(productEntity.getName());
        product.setCost(productEntity.getCost());
        product.setCount(productEntity.getCount());
        product.setIsAvailable(productEntity.getIsAvailable());
        productRepository.save(product);
    }

    @Override
    public void deleteProduct(UUID id) {
        productRepository.deleteById(id);
    }

    @Override
    public HashMap<ProductEntity, Integer> getAvailableFromInsertion(String insertion) {
        productRepository.getAvailableByInsertion(insertion);
        return null;
    }

    @Override
    public List<ProductEntity> getAvailableByMetal(String metal) {
        productRepository.getAvailableByMetal(metal);
        return null;
    }

}
