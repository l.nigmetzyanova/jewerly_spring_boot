package kpfu.itis.sem.service;

import kpfu.itis.sem.dto.request.WishDto;
import kpfu.itis.sem.dto.request.WishForm;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface WishService {

    List<WishDto> getWishes();

    WishDto createWish(WishForm wishForm);

    WishDto getWishById(UUID id);

    public WishDto updateWish(UUID id, WishForm wishForm);

    public void deleteById(UUID id);



}
