package kpfu.itis.sem.service;

import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.dto.request.OrderFormDto;
import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.OrderEntity;

import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
public interface OrderService {

    void createOrder(OrderEntity order);

    OrderEntity createEntityByOrderInfo(OrderFormDto orderInfo, HashMap<ProductsDto, Integer> cart);

    List<OrderEntity> getOrdersByCustomerId(UUID id);

    List<OrderDto> listToDto(List<OrderEntity> entities);

    List<OrderEntity> getAll();

    void updateStatusById(UUID orderId);
}
