package kpfu.itis.sem.service.impl;

import kpfu.itis.sem.dto.request.CustomerDto;
import kpfu.itis.sem.dto.request.OrderFormDto;
import kpfu.itis.sem.entity.CustomerEntity;
import kpfu.itis.sem.entity.UserEntity;
import kpfu.itis.sem.repository.CustomerRepository;
import kpfu.itis.sem.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseCustomerService implements CustomerService {

    private final CustomerRepository customerRepository;

    private final BaseUserService userService;



    @Override
    public void createCustomer(CustomerEntity customer) {
        customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(UUID id, String name, String phone) {
        CustomerEntity customerEntity = customerRepository.getReferenceById(id);
        customerEntity.setName(name);
        customerEntity.setPhoneNumber(phone);
        customerRepository.save(customerEntity);
    }

    @Override
    public CustomerEntity getCustomerByUserId(UUID id) {
        return customerRepository.getCustomerEntityByUserId(userService.getUserById(id));
    }

    @Override
    public CustomerEntity getCustomerById(UUID id) {
        return customerRepository.getReferenceById(id);
    }

    @Override
    public CustomerEntity createCustomerByNameAndPhone(String name, String phone) {
        CustomerEntity customerEntity = CustomerEntity.builder()
                .name(name)
                .phoneNumber(phone)
                .status("user")
                .build();
        return customerEntity;
    }

}
