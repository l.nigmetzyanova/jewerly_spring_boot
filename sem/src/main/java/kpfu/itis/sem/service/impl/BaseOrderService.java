package kpfu.itis.sem.service.impl;

import kpfu.itis.sem.dto.request.OrderDto;
import kpfu.itis.sem.dto.request.OrderFormDto;
import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.OrderEntity;
import kpfu.itis.sem.repository.OrderRepository;
import kpfu.itis.sem.service.OrderService;
import kpfu.itis.sem.util.mapper.impl.BaseListOrderMapper;
import lombok.RequiredArgsConstructor;
import org.hibernate.criterion.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseOrderService implements OrderService {

    private final OrderRepository orderRepository;

    private final BaseCartService cartService;

    private final BaseCustomerService customerService;

    private final BaseListOrderMapper orderMapper;


    @Override
    public void createOrder(OrderEntity order) {
        orderRepository.save(order);
    }

    @Override
    public OrderEntity createEntityByOrderInfo(OrderFormDto orderInfo, HashMap<ProductsDto, Integer> cart) {
        OrderEntity order = OrderEntity.builder()
                .address(orderInfo.getAddress())
                .comment(orderInfo.getComment())
                .cost(cartService.getSum(cart))
                .countProduct(cartService.getProductCount(cart))
                .customerName(orderInfo.getCustomerName())
                .status("В обработке")
                .build();
        return order;
    }

    @Override
    public List<OrderEntity> getOrdersByCustomerId(UUID id) {
        return orderRepository.getOrderEntitiesByCustomerId(customerService.getCustomerById(id));
    }

    @Override
    public List<OrderDto> listToDto(List<OrderEntity> entities) {
        return orderMapper.mupStruct(entities);
    }

    @Override
    public List<OrderEntity> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public void updateStatusById(UUID orderId) {
        OrderEntity order = orderRepository.findById(orderId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found"));
        order.setStatus("Доставлено");
        orderRepository.save(order);
    }

}
