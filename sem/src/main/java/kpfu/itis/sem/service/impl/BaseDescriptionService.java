package kpfu.itis.sem.service.impl;


import kpfu.itis.sem.dto.request.DescriptionForm;
import kpfu.itis.sem.entity.DescriptionEntity;
import kpfu.itis.sem.entity.ProductEntity;
import kpfu.itis.sem.repository.DescriptionRepository;
import kpfu.itis.sem.service.DescriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseDescriptionService implements DescriptionService {

    private final DescriptionRepository descriptionRepository;

    private final BaseProductService productService;



    @Override
    public DescriptionEntity getDescriptionByProductId(UUID id) {
        DescriptionEntity description = descriptionRepository.findByProductId(productService.getProductById(id)).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Description not found"));
        return description;
    }

    @Override
    public void save(DescriptionEntity description) {
        descriptionRepository.save(description);
    }

    @Override
    public void update(UUID id, DescriptionForm descriptionForm) {
        DescriptionEntity description = descriptionRepository.getReferenceById(id);
        description.setGender(descriptionForm.getGender());
        description.setInsertion(descriptionForm.getInsertion());
        description.setMechanism(descriptionForm.getMechanism());
        description.setMinWeight(descriptionForm.getMinWeight());
        description.setMetal(descriptionForm.getMetal());
        descriptionRepository.save(description);
    }

    @Override
    public List<DescriptionEntity> getDescriptions() {
        return descriptionRepository.findAll();
    }

    @Override
    public void deleteDescriptionById(UUID id) {
        descriptionRepository.deleteById(id);
    }

    @Override
    public DescriptionEntity getDescriptionByProductId(ProductEntity entity) {
        return descriptionRepository.getDescriptionEntitiesByProductId(entity);
    }

}
