package kpfu.itis.sem.service;


import kpfu.itis.sem.dto.request.ProductsDto;
import kpfu.itis.sem.entity.ProductEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface ProductService {

    List<ProductEntity> showAllProducts();

    void createProduct(ProductEntity productEntity);

    ProductEntity getProductById(UUID id);

    ProductsDto getProductsDtoById(UUID id);

    void updateProduct(UUID id, ProductEntity productEntity);

    void deleteProduct(UUID id);

    HashMap<ProductEntity, Integer> getAvailableFromInsertion(String insertion);

    List<ProductEntity> getAvailableByMetal(String metal);

}
