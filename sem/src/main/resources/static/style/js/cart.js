function minusProductInCart(button){
    const productId = button.getAttribute('data-id');

    console.log(productId);
    console.log("try minus");

    const csrfToken = document.querySelector('meta[name="_csrf"]').getAttribute('content');
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

    fetch('/cart/decreaseCount', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(productId)
    })
        .then(response => {
            if (response.ok) {
                window.location.reload();
                console.log('Одна единица товара успешно удалена из корзины');
            } else {
                console.error('Ошибка при удалении товара в корзину.');
            }
        })
        .catch(error => {
            console.error('Ошибка сети:', error);
        });
}

function plusProductInCart(button){
    const productId = button.getAttribute('data-id');
    console.log(productId);
    console.log("try plus");

    const csrfToken = document.querySelector('meta[name="_csrf"]').getAttribute('content');
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

    fetch('/cart/increaseCount', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(productId)
    })
        .then(response => {
            if (response.ok) {
                window.location.reload();
                console.log('Одна единица товара успешно добавлена в корзину');
            } else {
                console.error('Ошибка при добавлении товара в корзину.');
            }
        })
        .catch(error => {
            console.error('Ошибка сети:', error);
        });
}

function addProductInCart(button){
    const productId = button.getAttribute('data-id');
    console.log(productId);
    console.log("try add");

    const csrfToken = document.querySelector('meta[name="_csrf"]').getAttribute('content');
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

    fetch('/cart/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(productId)
    })
        .then(response => {
            if (response.ok) {
                // window.location.reload();
                console.log('Одна единица товара успешно добавлена в корзину');
            } else {
                console.error('Ошибка при добавлении товара в корзину.');
            }
        })
        .catch(error => {
            console.error('Ошибка сети:', error);
        });
}

function deleteCartItemFromCart(button){

    console.log("try delete full");

    const productId = button.getAttribute('data-id');

    const csrfToken = document.querySelector('meta[name="_csrf"]').getAttribute('content');
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

    fetch('/cart/remove', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(productId)
    })
        .then(response => {
            if (response.ok) {
                window.location.reload();
                console.log('Товар удален полностью из корзины');
            } else {
                console.error('Ошибка при удалении товара в корзину.');
            }
        })
        .catch(error => {
            console.error('Ошибка сети:', error);
        });
}