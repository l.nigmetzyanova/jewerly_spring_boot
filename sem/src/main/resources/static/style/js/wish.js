document.addEventListener("DOMContentLoaded", function() {
    findWishes();
});

function findWishes(){
    fetch('/wish/allWishes')
        .then(response => response.json())
        .then(data => {
            const tableBody = document.querySelector(".wish-table tbody");
            tableBody.innerHTML = "";

            data.forEach(wish => {
                const row = document.createElement("tr");
                row.innerHTML = `
                    <td>${wish.id}</td>
                    <td>${wish.wish}</td>
                `;

                tableBody.appendChild(row);
            })});
}

function addWish(){

    let form = document.getElementById("wishForm");

    let wish = document.getElementById('wish').value;

    const csrfToken = document.querySelector('meta[name="_csrf"]').getAttribute('content');
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

    let formData = {
        wish: wish
    };

    fetch('/wish/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(formData)
    })
        .then(function(response) {
            if(!response.ok){
                throw new Error('упс');
            } else {
                form.reset();
                findWishes();
            }
        })
        .then(function(data) {
            console.log('Ответ от сервера:', data);
        })
        .catch(function(error) {
            console.error('упс', error);
        });
}